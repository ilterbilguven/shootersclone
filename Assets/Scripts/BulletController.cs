﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{

	private Rigidbody2D rb;

	public float speed = 0;

	public float waitForRetrurn = 0;

	public GameObject parentGameObject;

	public PlayerController pc;

	public TriangleController otherTriangle;

	//public Vector2 bulletVelocity;


	private void OnEnable()
	{
		rb = GetComponent<Rigidbody2D>();

		transform.parent = null;

		StartCoroutine(Fire());
		
		rb.velocity = pc.rot.transform.up * speed;

		//bulletVelocity = rb.velocity;


	}

	private void FixedUpdate()
	{

		//bulletVelocity = rb.velocity;
		//transform.localPosition += Ppctransform.up * speed;
	}

	IEnumerator Fire()
	{
		yield return new WaitForSeconds(waitForRetrurn);
		ReturnBullet();
	}

	void ReturnBullet()
	{
		transform.parent = parentGameObject.transform;
		transform.position = parentGameObject.transform.position;
		transform.localPosition = Vector3.zero;
		transform.gameObject.SetActive(false);
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Triangle"))
		{
			otherTriangle = other.gameObject.GetComponent<TriangleController>();
		}

		if (other.CompareTag("Bullet"))
		{
			ReturnBullet();
			return;
		}

		if (other.gameObject != parentGameObject && otherTriangle.pc != pc)
		{
			ReturnBullet();
		}
	}

}
