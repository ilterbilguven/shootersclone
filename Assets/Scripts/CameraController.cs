﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

	public GameObject player;

	public Vector3 playerPos;

	// Update is called once per frame
	void FixedUpdate ()
	{
		playerPos = player.transform.position;

		transform.position = new Vector3(playerPos.x, playerPos.y, transform.position.z);
	}
}
