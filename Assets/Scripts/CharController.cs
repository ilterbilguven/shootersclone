﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController : MonoBehaviour {

	internal Rigidbody2D rb;
	[SerializeField]
	internal Vector2 rawInput;
	[SerializeField]
	internal float speed;
	[SerializeField]
	internal float angle;

	public int triangleCount;

	public delegate void ButtonAction();

	public GameObject rot;


	[SerializeField]
	internal List<GameObject> Triangles = new List<GameObject>();
}
