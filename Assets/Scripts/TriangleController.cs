﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class TriangleController : MonoBehaviour
{
	public PlayerController pc;

	[SerializeField]
	internal Vector2 rawInput;
	[SerializeField]
	internal float angle;

	public GameObject myBullet;
	public GameObject otherBullet;


	private void OnEnable()
	{
		//var lastPos = pc.Triangles.Last().transform.localPosition;
		var randomPos = new Vector2(Random.value, Random.value);

		transform.localPosition = randomPos * 3;

		pc.triangleCount++;
		if (!pc.Triangles.Contains(transform.gameObject))
		{
			pc.Triangles.Add(transform.gameObject);
		}
		
		pc.Shoot += Shoot;
	}

	private void OnDisable()
	{
		pc.triangleCount--;
		pc.Shoot -= Shoot;
		pc.updateSlider();;
		
	}

	private void Shoot()
	{
		myBullet.SetActive(true);
	}

	void FixedUpdate()
	{
		rawInput.x = CrossPlatformInputManager.GetAxis("Horizontal");
		rawInput.y = CrossPlatformInputManager.GetAxis("Vertical");
		
		angle = Mathf.Atan2(rawInput.x, rawInput.y) * Mathf.Rad2Deg;
		
		transform.rotation = Quaternion.Euler(0, 0, -angle);
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Bullet"))
		{
			otherBullet = other.gameObject;
		}

		if (other.gameObject != myBullet && !other.CompareTag("Triangle") && otherBullet.GetComponent<BulletController>().pc != pc)
		{
			gameObject.SetActive(false);
		}
		
	}
}
