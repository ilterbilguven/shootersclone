﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : CharController
{
	
	public event ButtonAction Shoot;

	public Slider demoSlider;


	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		rawInput = new Vector2();

		updateSlider();

	}

	private void Update()
	{
		
		if (CrossPlatformInputManager.GetButtonDown("Shoot"))
		{
			if (Shoot != null) Shoot();
		}

		if (demoSlider.value > triangleCount)
		{
			if (Triangles.Count > triangleCount)
			{
				Triangles.Find(o => o.activeSelf == false).SetActive(true);
			}
			if (Triangles.Count < demoSlider.value)
			{
				Instantiate(Triangles.Find(o => o.activeSelf && o.transform.childCount > 0), transform);
			}
		}
		if (demoSlider.value < triangleCount)
		{
			Triangles.Find(o => o.activeSelf).SetActive(false);
		}

		
	}

	void FixedUpdate()
	{
		rawInput.x = CrossPlatformInputManager.GetAxis("Horizontal");
		rawInput.y = CrossPlatformInputManager.GetAxis("Vertical");

		angle = Mathf.Atan2(rawInput.x, rawInput.y) * Mathf.Rad2Deg;

		rot.transform.rotation = Quaternion.Euler(0, 0, -angle);


		rb.velocity = rot.transform.up * speed;
	}

	private void LateUpdate()
	{
		if (triangleCount <= 0)
		{
			Debug.Log("Game Over");
		}
	}

	public void updateSlider()
	{
		demoSlider.value = triangleCount;
	}
}
